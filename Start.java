/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    Start.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


import src.JFlightWizard;
import java.awt.GridLayout;
import java.awt.Dimension;
import javax.swing.JFrame;


/**
 *JFlightWizard startup.
* @see 
*  <A HREF="uml/JFlightWizard.jpg">Complet Class Diagram</A>
* @see 
*  <A HREF="uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../README">README</A>
 */

public final class Start extends JFrame{

//Creates the JFlightWizard JPanel.
	private final static  JFlightWizard jfw  = new JFlightWizard();


 	private String output = jfw.toString();


/**
*Adds the JFlightWizard JPanel to this Frame.
*/
	public Start( ){
		
		setLayout(new GridLayout(1,1));
		setTitle("JFlightWizard");
		add(jfw);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}


/**
 *Returns the last configuration, found in the configuration file.
* @return Returns a string representation of the object
 */
	public String toString(){
		return output;
	}


/**
 *Main program.
*Creates the application frame, sets the frame size to 1020x700 and prints out the last configurations.
 * @param args 
 */
	public static void main(String[] args){
		
		Start s = new Start();
		s.setSize(new Dimension(1020,700));
		s.setResizable(false);
		s.setVisible(true);
		System.out.println(s.toString());
	}
}
