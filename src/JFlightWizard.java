/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    JFlightWizard.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with  JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/

package src;

import java.io.File;


import java.awt.Image;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Point;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.ImageIcon;
import src.obj.Data;
import src.action.ParseProperties;
import src.action.CreateMenu;
import src.action.StartFlight;
import src.comp.ButtonPanel;
import src.comp.Menu;
import src.comp.MenuList;

/**
*  Initializes the  main functions and main components.
* @see 
*  <A HREF="../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../uml/seq/JFlightWizard.jpg">JFlightWizard Sequence Diagram</A>
* @see 
*  <A HREF="../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../README">README</A>
*/

public final class JFlightWizard extends JPanel {

	

	static public final  StartFlight start_flight = new StartFlight();
	static public  final CreateMenu create_menu = new CreateMenu();
	static public  final Menu menu = new Menu();
	static public final ParseProperties parse_properties = new ParseProperties();
	
	static public   JPanel panel_west = new JPanel();
	static public MenuList menu_list;
	
	static  private JFlightWizard this_jflightwizard;
	static  private  Image background = getBackgroundImage();
	static private final ButtonPanel buttons_panel = new ButtonPanel();
	
	static private Point buttons_location = new Point(10,600);
	private String output = parse_properties.toString()+"\n";



/**
*Creates all  main components of the GUI. 
*Specifies the Layout for placing the components. 
 */
	public  JFlightWizard (){           

		this.this_jflightwizard = this;


		menu_list = new MenuList(
								Data.order(
									Data.getMenuKeys()),
										Data.getCellRenderer());
		
		panel_west.setOpaque(false);	


		//Defines the Layout for the components.
		setLayout(null);
		panel_west.setBounds(
			0,
			0,
			180,
			700);

		menu.setBounds(
			180,
			5,
			830,
			667);	



		panel_west.setLayout(null);
		menu_list.setBounds(
			5,
			5,
			175,
			245);

		buttons_panel.setBounds(
			(int) buttons_location.getX()-2,
			(int) buttons_location.getY()-2, 
			146,
			 61);	


		
		//Places the Layout for the components.
		panel_west.add(menu_list);
		panel_west.add(buttons_panel);

		add(panel_west);
		add(menu);
		


	}

	//Returns the Image associated with  value of "--extra-background="  if it exists.
	static private final  Image getBackgroundImage(){
		String path = null;
		if ((path = Data.getValue("--extra-background="))!= null){
			Image i = null;		
			if (new File(path).exists())
				i =  new ImageIcon(path).getImage();
			return i;
		}
		else return null;	
	}




/**
 *Resets the buttons_location value.
*/	
	static public void resetButtonsLocation(){
		buttons_location.setLocation(buttons_panel.getLocation());
	}

		
/**
 *Adds an AddOn buttons panel.
 *The height of the buttons panel must be < 325 or the location of the  buttons panel will be the location of the last button panel.
 * @param  bp JPanel. 
 */	
	static public void setAddOnButtons(JPanel bp){
		//Sets a new buttons_location value.
		buttons_location.setLocation(
			buttons_location.getX(),
			buttons_location.getY()- (int) bp.getSize().getHeight());
	
		//Sets the location of the addon buttons panel to the new buttons_location value.
		//The y value of the buttons panel location must be > 275 else the buttons panel will be placed to the last buttons panel location.
		if ( buttons_location.getY() > 245 )
			bp.setLocation(buttons_location);

		panel_west.add(bp);
	}


	
/**
*Loades the  background image
*This methode also invoke the  paintComponent methode of the JFlightWizard JPanel.
 */	
	static public void repaintBackground(){
		background = getBackgroundImage();
		this_jflightwizard.repaint();
	}




/**
 * Paints the background.
 * @param  g Graphics object 
 */
	public final  void paintComponent(Graphics g){
		if (background != null)
			g.drawImage(background,0,0,this.getWidth(),this.getHeight(), this);
		else{
			g.setColor(Color.black);		
			g.fillRect(0,0,this.getWidth(),this.getHeight());
		}
	}


/**
 *Returns the last configuration, found in the configuration file.
* @return returns a string representation of the object
 */
	public String toString(){
		return output;

	}
}
