/**
 *Initialize the main application.
* @see 
*  <A HREF="../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../uml/seq/JFlightWizard.jpg">JFlightWizard Sequence Diagram</A>
* @see 
*  <A HREF="../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../README">README</A>
 */
package src;
