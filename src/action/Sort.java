/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    Sort.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.action;

import src.obj.Data;

/**
* Sorting (not ready now).
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/Sort.sort.jpg">Sort.sort Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class Sort{


/**
*Sorts Strings in order of  the first character.
 * @param dat Array of unsorted objects (Strings).
* @return Array of sorted objects. 
*/
	public Object[] sort(Object[] dat){                                   
		String sortorder = "0123456789-abcdefghijklmnopqrstuvwxyz_[]";
		String[] sorted = new String[dat.length];
		int so = 0;

		for(int i = 0;i<sortorder.length();i++){	
			for (Object o : dat){
				String s = (String)o;
				if (s.toLowerCase().charAt(0) == sortorder.charAt(i)){
					sorted[so] = s; 
					so++;
				}
			}
		}

		return (Object[]) sorted;
	}


/**
*Order Strings and replace the numbers in the Strings  if  the first characters are integers.
 * @param dat Array of unordered objects (Strings).
* @return Array of ordered objects. 
*/
	public Object[] order(Object[] dat){
		String[] ordered = new String[dat.length];
		String s = null;
		try{
			for(int i = 0;i< ordered.length; i++){
				 s = (String) dat[i];
				int index = Integer.parseInt(
								s.substring(
									0, s.indexOf(".")));

				ordered[index-1] = 
					s.substring(
						s.indexOf(".")+1, s.length());
				Data.setMenu(ordered[index-1], Data.getMenu(s));
				Data.removeMenu(s);

			}
		}
		catch(NumberFormatException e){System.out.println("format problem:           "+s);}
		catch(IndexOutOfBoundsException x){System.out.println("format problem:        " +s);}
		return ordered;
	}





}
