/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


   DebugOutput.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/



package src.action;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;


/**
* Creates a Thread for printing FlightGears terminal output to stdout.
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/DebugOutput.jpg">DebugOutput Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class DebugOutput implements Runnable{

	private Process p;



/**
 *Initializes DebugOutput. 
 * @param p Process the current running FlightGear is associated with. 
*/		
	protected DebugOutput(Process p){
		this.p = p;
	}


	
/**
 * While FlightGear is running it's terminal output will be redirected to stdout by this method. 
*/
	public void run(){
		String er = "";
		String in = "";
		 try {
			BufferedReader error = new BufferedReader(
								new InputStreamReader(p.getErrorStream()));
			BufferedReader out = new BufferedReader(
								new InputStreamReader(p.getInputStream()));
			while ((er = error.readLine()) != null || (in = out.readLine()) != null) {
			 	if (er != null)System.out.println(er);
				if (in != null)System.out.println(in);
			}
		 } catch (IOException e) {System.out.println(e);}	
	}
}
