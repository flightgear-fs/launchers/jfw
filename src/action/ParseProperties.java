/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    ParseProperties.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.action;

import java.util.ArrayList;
import javax.swing.ButtonGroup;

import src.obj.PropertiesObject;
import src.obj.Data;
import src.action.GetPlugin;



/**
*  Parses  the  properties.txt and configuration file, fills the Data.menu HashMap with PropertiesObjects and  the values HashMap with the last configuration.
* To manage the cration of menu components  it is possible to use some tags in properties.txt file. 
* "$M$" instructs this class to create a new  MenuList item. The String representation of this MenuList item will be a key for the menu Hashmap. 
*"$P$" instructs  this parser to creat a GetPlugin class loader for searching the addon package for a class associated with the name  next to the "$P$" tag. 
*"$Start$" creates a Buttongroup for options wich debend to other options.
*"$R$" This is needed to mark the debendend options. Every command line option marked with "$R$" will be conected to the last created ButtonGroup.
* For customizing the file chooser dialogues behaviour of the BrowseTextField menu component it is possible to use 3 tags in properties.txt file. 
* "$BROWSE$" instructs the CreateMenu().create() method to create a BrowseTextField menu item. 
*"$SEP$" instructs  an BrowseTextField menu item to handl more than one value in one TextField.
*"$ONLYDIR$" instructs the dialogue to show only directories.
*All entries, of the properties.txt fiel, without a tag will be added to the standard menu components.
*The parser associates TextFields with commant line options containig a "=", or CheckBoxes with "--en" and everything else containing "--".   
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/class/Extensions.jpg">Extensions  Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/ParseProperties.jpg">ParseProperties Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/ParseProperties.parseConfig.jpg">ParseProperties.parseConfig Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/
public class ParseProperties{
	
	private ButtonGroup bg = null;
	private String menu_name = "";
	private String[] opt = {"$M$", "$P$","--","$Start$"};
	private ArrayList<String> properties =Data.read(Data.UD+Data.FSEP+"propertielist.txt",opt);
	private ArrayList<String> values;
	private String output; 


/**
 *Parses  the  properties.txt and config.txt file. Adds the properties to the correct menu. Sets the correct propertie values found in config.fgc.
 */	
	public  ParseProperties(){

	//Parses  the  properties.txt		
		for(String p: this.properties){
			p = p.trim();

		//new menu .
			if(p.contains("$M$"))
				addMenuItem(p);

		// plugin  item.
			else if(p.contains("$P$")) 
				addPluginItem(p);

		// create ButtonGroup.
			else if(p.contains("$Start$")) 
				this.bg = new ButtonGroup();

			else{

			//split command and text.
				String[] value = p.split("#");

			// add new Radiobutton item to the last created ButtonGroup.
				 if(p.contains("$R$"))					
					addRadioButtonItem(value);
				
				
			// BrowsTextField item.
				else if(p.contains("$BROWSE$"))
					addBrowsTextFieldItem(value);

			//ComboBox  item
				else if(p.contains("$COMBO$"))
					addComboBoxItem(value);

			//SpinnerPanel  item
				else if(p.contains("$SPINNER$"))
					addSpinnerPanelItem(value);
					
				else if(p.contains("--")){

				// TextField  item. 
					if(value[0].contains("=")) 
						addTextItem(value);
			
				// CheckBox item. 
					else if(value[0].startsWith("--en")) 
						addCheckBoxItem(value);
	
				// CheckBox  item (all properties not containing disable, enable or =  in the command).
					else 
						addCheckBoxOptionItem(value);
				}
			}
		}
		
	//parses the default config in the JFlightWizard root directory.
		parseConfig(Data.getDefaultConfig());

 	}




// Filters out the FlightGear command from the properties command string.
	private String getCommand(String c){
		return c.substring(c.lastIndexOf("$")+1, c.lastIndexOf("=")+1).trim();
	}

//Create an repertory of values fromthe tag found in properties.txt.
	private String[] getRepertory(String c, int si){
		String[] tmp = c.substring(si, c.lastIndexOf("$")).split(",");
		String[] items = new String[tmp.length +1];
	
		for (int i = 1; i < items.length; i++){
			items[i] = tmp[i-1].trim();
		}
		return items;
	}







	

//Adds a new plugin Properties_Object  to a menu in the  menu HashMap in the Data object.
	private void addPluginItem(String p){
		GetPlugin gp = new GetPlugin(p.substring(p.lastIndexOf("$")+1, p.length()));
		Data.getMenu( menu_name).add(new  PropertiesObject(null,  gp));
	}


//Creates new CheckBox  for command line options with enable/disable ..
	private void addCheckBoxItem(String[] c){
		String command = c[0].substring(c[0].indexOf("able-")+5, c[0].length());
		addToMenu(command, c[1]);
	}

//Creates new CheckBox  for command line options without enable/disable .
	private void addCheckBoxOptionItem(String[] c){
		String command = c[0].substring(c[0].lastIndexOf("--"), c[0].length());
		addToMenu(command, c[1]);
	}


//Creates new BrowsTextField
	private void addBrowsTextFieldItem(String[] c){
		
	//Variables for dialogue behaviour
		boolean[] db = {false, false};
		if(c[0].contains("$SEP$")) 
			db[0] = true;
		if(c[0].contains("$ONLYDIR$")) 
			 db[1] = true;

		
		ArrayList<String[]> items = new ArrayList<String[]>();
		String[] tmp =c[0].split("[$][ ]*[$]");
		for (String s : tmp){
			if (s.contains("FILEFILTER:")){
				if (!s.contains("$"))s = s +"$";
System.out.println( s);
				String[] ff = getRepertory(s, 11);
				ff[2] = ff[2].replace(":",",");
System.out.println( ff[1]);
System.out.println( ff[2]);
				items.add(ff);
			}
		}

		
	//get the command
		String	command = getCommand(c[0]);
		Data.getMenu( this.menu_name).add(new  PropertiesObject(command, db, items.toArray(), c[1]));
	}


//Creates new SpinnerPanel property
	private void addSpinnerPanelItem(String[] c){
		ArrayList<String[]> items = new ArrayList<String[]>();
		String[] spinners =c[0].split("[$][ ]*[$]");
		String[] params;
				
		for(int i = 1; i < spinners.length; i++){
			String mode = null;
			String s = spinners[i];
			int start_index = 0;
		//check mode
			if (s.startsWith("INT:")){
				mode = "$INT$";
				start_index = s.indexOf("INT:")+4;
			}
			else if (s.startsWith("DOUBLE:")){
				mode = "$DOUBLE$";
				start_index = s.indexOf("DOUBLE:")+7;
			}
			else if (s.startsWith("DATE:")){
				mode = "$DATE$";
				start_index = s.indexOf("DATE:")+5;
			}
		//if this spinner belongs to a ButtonGroup 
			if (s.startsWith("BELONGTO:")){
				mode = "$BELONGTO$";
				start_index = s.indexOf("BELONGTO:")+9;
			}				
		//get the parameter
			if (!s.contains("$"))s = s +"$";
			params = getRepertory(s, start_index);
			params[0] = mode;
			items.add(params);
		}
	
	//get the command
		String	fgcommand = getCommand(c[0]);
		Data.getMenu( this.menu_name).add(new  PropertiesObject("spinnerpanel", fgcommand,  items.toArray(), c[1]));
	}


//Creates new ComboBox property
	private void addComboBoxItem(String[] c){
	//get the parameter
		int start_index = c[0].indexOf("$ITEMS:")+7;
		String[] items = getRepertory(c[0], start_index);			
	//get the command
		String	fgcommand = getCommand(c[0]);
	//if multible selection is set for this property		
		if(c[0].contains("$MULTI$"))
			fgcommand = "--multi"+fgcommand;
		Data.getMenu( this.menu_name).add(new  PropertiesObject("combobox", fgcommand, items, c[1]));
	}


//Creates new   RadioButton  command and text.
	private void addRadioButtonItem(String[] c){
	//if a menu component belongs to this propertie
		String linked_component = null;
		if (c[0].contains("$BELONGTO:")){
			linked_component = c[0].substring(c[0].indexOf("$BELONGTO:")+10, c[0].lastIndexOf("$"));
		}
		String command = c[0].substring(c[0].lastIndexOf("--"), c[0].length());
		Data.getMenu( this.menu_name).add(new  PropertiesObject(command.trim(), linked_component, c[1].trim(), this.bg));
	}

//Creates  new TextField  command and text.
	private void addTextItem(String[] c){
		String command = c[0].substring(0, c[0].lastIndexOf("=")+1);
		addToMenu(command, c[1]);
	}






//Adds a new item to the menu HashMap in the Data object.
	private void addMenuItem(String m){
		this.menu_name = m.substring(m.lastIndexOf("$")+1,m.length());
		Data.setMenu( menu_name, new ArrayList<PropertiesObject>());
	}

//Adds a new Properties_Object object, containing  command and text,  to a menu in the  menu HashMap in the Data object, dependent on the isbutton_group variable.
	private void addToMenu(String command, String text){
			Data.getMenu( this.menu_name).add(new  PropertiesObject(command.trim(),  text.trim()));
	}





//adds last TextField value to the values HashMap in the Data object.
	private void setTextValue(String v){
		String com = v.substring(0, v.indexOf("=")+1).trim();
		String val =  v.substring( v.indexOf("=")+1, v.length()).trim();
	//add values as multible, if there are more values, for the same option, where found.
		if (Data.containValueKey(com)){
			val = Data.getValue(com)+Data.PSEP+val;
			Data.removeValue(com);
			com = "--multi"+com;
		}
		else if(Data.containValueKey("--multi"+com)){
			com = "--multi"+com;
			val = Data.getValue(com)+Data.PSEP+val;
		}
		
		Data.setValue(com, val);
	}

//adds last CheckBox value to the values HashMap in the Data object.
	private void setCheckBoxValue(String v){
		Data.setValue(v.substring(v.lastIndexOf("able-")+5,v.length() ).trim(), v.substring(0, v.lastIndexOf("able-")+5 ).trim());
	}

//adds last CheckBox  or RadioButton value to the values HashMap in the Data object.
	private void setOptionValue(String v){
		Data.setValue(v.substring(v.lastIndexOf("--"), v.length() ).trim(), v.trim());
	}


/**
 *Returns the configuration, found in the file associated with the path.
* @param path full path to the configuration file.
 */
	public void parseConfig(String path ){
		Data.clearValue();		
		output =  "\n\n  Current Configuration:\n-----------------------------\n";
		values = Data.read(path,null);
		
	//Parses  the configuration file file. 	
		for(String v : this.values){
			output = output.concat(v+"\n");
			v=v.trim();

		//add  TextField, CheckBox, Radiobutton values.
			if(v.contains("=")) setTextValue(v);
			else if(v.startsWith("--dis") ||
				 v.startsWith("--en")) setCheckBoxValue(v);
			else if (!v.equals("")) setOptionValue(v);
		}

	//Setsthe variable, for checking whether the configuration values have changed or not, to false.
		Data.setDataChanged(false);
	}

/**
 *Returns the configuration, found in the configuration file.
* @return returns a string representation of the object
 */
	public String toString(){
		return output;
	}
}
