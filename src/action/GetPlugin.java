/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


   GetPlugin.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.action;

import javax.swing.JPanel;

import src.addon.AbstractAddOn;



/**
* Class loader for Add-Ons.
* @see 
*  <A HREF="../../uml/class/Extensions.jpg">Extensions  Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/GetPlugin.jpg">GetPlugin Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public  class GetPlugin{      
                                                   // search for specific pluginname
	private AbstractAddOn addon;
	


/**
 *Initializes GetPlugin.  Searches the src.addon package for the class associated with the classname variable.
 * @param classname name of the Add-On class found in the properties.txt file. 
*/		
	protected  GetPlugin(String classname){

		String path = "src.addon."+classname.trim();
		
		try{
			Class<?> c =  Class.forName(path);
			addon = (AbstractAddOn)c.newInstance();
		} 
		catch (Exception exception){exception.printStackTrace();}

	}


/**
 *Returns a JPanel that represents the add-on.
 * @return JPanel. 
*/	
	protected JPanel getPanel(){return (JPanel) this.addon.getPanel();}

/**
 *Returns a JPanel that contains the buttons.
 * @return JPanel.  
 */	
	public JPanel getButtonsPanel(){return (JPanel) this.addon.getButtonsPanel();}

}



					
