/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    CreateMenu.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.action;

import java.util.ArrayList;
import java.util.Enumeration;

import java.awt.LayoutManager;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JComponent;

import src.comp.Panel;
import src.comp.SpinnerPanel;
import src.comp.TextField;
import src.comp.CheckBox;
import src.comp.ComboBox;
import src.comp.RadioButton;
import src.comp.BrowseTextField;
import src.obj.PropertiesObject;
import src.obj.Data;
import src.JFlightWizard;


/**
* Creates an Array of objects, containing all the menu components associated with the selected MenuList item. 
*The ParseProperties class initialices all command line options found in the properties.txt file and stores every option as an PropertiesObjet in the menu Hashmap.
*The create() methode of this class initializes all the menu components with the information provided by the PropertiesObjects and the configuration values from the values HashMap.
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/class/Extensions.jpg">Extensions  Class Diagram</A>
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/CreateMenu.create.jpg">CreateMenu.create Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class CreateMenu {  

	private ArrayList<Panel> options_panel;


/**
 * Creates all TextFields,BrowseTextFields with defined dialugue behaviour , CheckBoxes RadioButtons and AddOns defined in the dat array. 
 * @param dat  an Array of Objects containing PropertiesObjects, depending on wich MenuList item was selected.
* @return  an Array of objects containing all property Panels.  
 */
	public Object[] create(Object[] dat){       
               
		this.options_panel = new ArrayList<Panel>();

	// Clear all ButtonGroups.
		for(Object o :  dat){
			PropertiesObject po = (PropertiesObject)o;
			if (po.getButtonGroup() != null)
				clearButtonGroup(po.getButtonGroup());
		}

	// Create menu items.
		for(Object o :  dat){

			PropertiesObject po = (PropertiesObject)o;
			String text = po.getText();
			String command = po.getCommand();
			
	//SpinnerPanel component
			if(command.equals("spinnerpanel")){
				createSpinnerPanelItem(
					createPanel(new FlowLayout(FlowLayout.LEFT),false), 
						po.getFGCommand(),
							po.getItems(), 	
								"    "+text);
			}


		//ComboBox component
			else if(command.equals("combobox")){
				createComboBoxItem(
					createPanel(new FlowLayout(FlowLayout.LEFT), false), 
						po.getFGCommand(),
							po.getItems(), 	
								 "    "+text);

			}

		//BrowseTextField component
			else if(command.equals("browstextfield")){
				createBrowseTextFieldItem(
					createPanel(null, false), 
						po.getFGCommand(), 	
							"    "+text,
								po.isMulti(),
									po.isOnlyDir(),
										po.getFileFilter());
			}


		//Add-On component
			else if (command.equals("plugin")){
				createPluginItem(
					createPanel(new BorderLayout(), true), 
						po.getPlugin().getPanel(),
							po.getPlugin().getButtonsPanel(), 	
								BorderLayout.CENTER);

			}


		//RadioButton component
			else if (command.equals("radiobutton")){
			
				createRadioButtonItem(
					createPanel(new FlowLayout(FlowLayout.LEFT), false), 
						po.getFGCommand(), 
							hasValue(po.getFGCommand()), 
								po.getButtonGroup(),
						 			 "    "+text,
										po.getSpinner());
			}

			
		//TextField component
			else if(command.contains("=")){
				createTextFieldItem(
					createPanel(new FlowLayout(FlowLayout.LEFT), false), 
						command, 	
							 "    "+text);
			}


	

		//CheckBox component
			else{
				createCheckBoxItem(
					createPanel(new FlowLayout(FlowLayout.LEFT), false), 
						command,
							hasValue(command),
								"    "+ text);
			}
		}
		return options_panel.toArray();
	}




// Clear  ButtonGroup.
	private void clearButtonGroup(ButtonGroup bg ){
		for (Enumeration<AbstractButton>  b = bg.getElements(); b.hasMoreElements();)
       			bg.remove(b.nextElement());
	}

//Checks whether the RadioButton or CheckBox is selected or not.
	private boolean hasValue(String command){
		if (Data.getValue(command) != null ){
			boolean hv = false;
			if (!Data.getValue(command).equals("--disable-"))
				hv = true;

			return hv;
		}
		else
			return false;
	}

// Creates a new Panle for adding menu components to it.   
	private Panel createPanel(LayoutManager lm, boolean isplugin){
		this.options_panel.add(new Panel( lm,  isplugin));
		return (Panel)this.options_panel.get(this.options_panel.size()-1);

	}
	


	private void createSpinnerPanelItem(Panel p, String command, Object[] items, String text){
		String value = Data.getValue(command);
		p.add(new SpinnerPanel(value, command, items));
		p.add(new JLabel(text));
		p.getComponent(1).setForeground(Data.getFontColor());
	}


	private void createComboBoxItem(Panel p, String command, Object[] items, String text){
		String value = Data.getValue(command);
		p.add(new ComboBox(value, command, items));
		p.add(new JLabel(text));
		p.getComponent(1).setForeground(Data.getFontColor());
	}


	private void createBrowseTextFieldItem(Panel p, String command, String text, boolean separation, boolean only_dir, Object[] file_filter){
		String values = Data.getValue(command);
		p.add(new BrowseTextField(values,command, 30, separation, only_dir, file_filter));
		p.getComponent(0).setBounds(0,0,420,18);
		p.add(new JLabel(text));
		p.getComponent(1).setBounds(425,0,600,18);
		p.getComponent(1).setForeground(Data.getFontColor());
	}


	

	private void createTextFieldItem(Panel p, String command, String text){
		String values = Data.getValue(command);
		p.add(new TextField(values,command, 30));
		p.add(new JLabel(text));
		p.getComponent(1).setForeground(Data.getFontColor());
	}


	private void createPluginItem(Panel p, JPanel plugin_panel, JPanel buttons_panel,  String pos){
		p.add( plugin_panel,  pos);
		p.setButtonsPanel(buttons_panel);
	}


	private void createRadioButtonItem(Panel p, String command, boolean selected, ButtonGroup bg, String text, String spinner){
		p.add(new RadioButton(text, command , selected, bg, spinner));
	}


	private void createCheckBoxItem(Panel p, String command,  boolean selected, String text){
		p.add(new CheckBox(text,command, selected ));
	}
}
