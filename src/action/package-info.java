/**
 *Offers everything so that the application can do it's job.
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/class/Extensions.jpg">Extensions  Class Diagram</A>
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/GetPlugin.jpg">GetPlugin Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/StartFlight.saveConfig.jpg">StartFlight.saveConfig Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/StartFlight.executeCommand.jpg">StartFlight.executeCommand Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/StartFlight.createCommand.jpg">StartFlight.createCommand Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/Sort.sort.jpg">Sort.sort Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/ReadWriteFile.write.jpg">ReadWriteFile.write Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/ReadWriteFile.read.jpg">ReadWriteFile.read Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/ParseProperties.jpg">ParseProperties Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/ParseProperties.parseConfig.jpg">ParseProperties.parseConfig Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/DebugOutput.jpg">DebugOutput Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/CreateMenu.create.jpg">CreateMenu.create Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
 */
package src.action;
