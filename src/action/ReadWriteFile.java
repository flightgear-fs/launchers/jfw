/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    ReadWriteFile.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/







package src.action;

import java.io.LineNumberReader;
import java.io.FileReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.zip.GZIPInputStream;
import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.IOException;


import src.obj.Data;


/**
* Reads text or zip files. Overwrite  files or append text to it.
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/ReadWriteFile.write.jpg">ReadWriteFile.write Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/ReadWriteFile.read.jpg">ReadWriteFile.read Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class ReadWriteFile{
	

	public File unzip(File p) {
		String n = p.getName();
		String outfile = Data.TMPDIR+Data.FSEP+n.substring(0,n.indexOf(".gz") );
		try{
   			GZIPInputStream  in = new GZIPInputStream(new FileInputStream(p));
    			OutputStream out = new FileOutputStream(outfile);
       			byte[] buf = new byte[1024];
      			int len;
      			while ((len = in.read(buf)) > 0) {
        			out.write(buf, 0, len);
      			}
     			out.close();
			in.close();
		}	
		catch (FileNotFoundException d) {System.out.println(n +" not exists!!\n");}
		catch (IOException a) {System.out.println(n +" not readable!!\n");}
		return new File(outfile);
  	}




/**
*Write the content of data into a file.
* @param path full path of the file.
* @param data  Array of Strings wich will be written into the file.
* @param append if true the new content will be append to the old, else the old content will be overwritten.
*/
	public void write(String path, Object[] data, boolean append){

		try{	
			FileWriter writefile = new FileWriter(new File(path), append);

			for (int i = 0; i<data.length;i++){
				if (data[i] != null && 
					!data[i].toString().contains("--wp=") && 
						!data[i].toString().contains("--flight-plan=")){
					String tmp = (String)data[i];
					writefile.write(tmp.concat("\n"));
				}
			}
			writefile.close();
		}catch (IOException x){System.out.println("Can't write!!!!!!!!!!!");}

	}




/**
*Reads a zip or text file  and returns every line containing one or more Strings defined in the Array.
*If the array is null the whole  content of the file will be returned.
* @param pf full path of the file.
* @param toread  Array of Strings for defining the return result.
* @return ArrayList<String> contains the result Strings.
*/
	public ArrayList<String> read(String pf,String[] toread){       
		String found = "";
		File pfad = new File(pf);
		ArrayList<String> foundrady = new ArrayList<String>();
		
		try{
			LineNumberReader searchinfile = null;
			if (pfad.getName().endsWith(".gz"))searchinfile =  new LineNumberReader(new FileReader(unzip(pfad)));
			else searchinfile = new LineNumberReader(new FileReader(pfad));
			while ((found = searchinfile.readLine()) != null ){
				if (toread != null){
					for (String s : toread){
						if (found.contains(s)){
							foundrady.add(found);
						}
					}
				}
				else foundrady.add(found);
			}
			searchinfile.close();
		}
		catch (FileNotFoundException d) {System.out.println(pfad +" not exists!!\n");}
		catch (IOException a) {System.out.println(pfad +" not readable!!\n");}
		return foundrady;
	}
}
