/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    StartFlight.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/





package src.action;


import java.util.ArrayList;

import java.io.IOException;

import src.obj.Data;
import src.action.DebugOutput;


/**
* Creates, executes and saves the command for starting FlightGear .
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/StartFlight.saveConfig.jpg">StartFlight.saveConfig Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/StartFlight.executeCommand.jpg">StartFlight.executeCommand Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/StartFlight.createCommand.jpg">StartFlight.createCommand Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class  StartFlight{


	private ArrayList<String> options = new ArrayList<String>();
	private ArrayList<String> extra = new ArrayList<String>();
	private String execute = "fgfs";
	private String output = " ";
	private Thread t = null;

/**
 *Creates the executable command for FlightGear startup. 
* This method checks whether the value for an option contains the multible target "#".
* If the "#" was found multible instances of this option will be created by splitting the value at the "#" positins.
* @return an Array of Strings containing the path to the executable and the defined command line options.
* @exception java.lang.NullPointerException NullPointerException.
 */
	public String[] createCommand() throws NullPointerException{
		
		this.output = " ";

		//Clears the ArrayLists if they are not empty.
		if(!options.isEmpty())options.clear();	
		if(!extra.isEmpty())extra.clear();

		//Adds the path of FlightGears executeable to the top off the options ArrayList 
		if ( Data.containValueKey("--fg-execute=")){
			if (Data.getValue("--fg-execute=") != null)
				this.execute = Data.getValue("--fg-execute=").trim();
		}
		this.options.add(this.execute);

		//Creates an Array containing the keys for all defined values in the values HashMap. 
		Object[]  keys = Data.getValueKeys();
		
		for (int i=0;i<keys.length;i++){
			String value = (String) Data.getValue(keys[i]).trim();
			
			if(value != null &&	!value.equals(this.execute)){	

				//Checks whether the option contains a multible value or not
				String c = keys[i].toString();
				if (c.contains("--multi")){
					String[] multi = value.split(Data.PSEP);
					c = c.substring(c.indexOf("--multi")+7, c.length());
					for(String s :  multi){ options.add(c+s); }
				}

				// If 	the value is an TextField value.
				else if (keys[i].toString().contains("=") 
					&& !keys[i].toString().contains("--extra"))			
						 this.options.add( keys[i]+ value);
			


				// If 	the value is an CheckBox value.
				else if (value.contains("able-") && !keys[i].toString().contains("--extra"))
						this.options.add( value + keys[i]);


				// If 	the value is an Option value.
				else if (value.charAt(0) == '-' && !keys[i].toString().contains("--extra") )
						this.options.add(value);



				// If 	the value is an extra-TextFild  value for none FlightGear options .
				else this.extra.add(keys[i]+value);         
			}
		}


		// Creates an Array of Strings for executing.
		String[] command = new String[this.options.size()];
		for(int i=0;i<options.size();i++){
			command[i] = options.get(i);
		}


		// Creates the output String containing the  command for FlightGear startup.
		String st =" ";
		for(String c : command ){
			st = st.concat("\n "+c);
		}
		this.output = this.output.concat(st+"\n");

		return command;
	}



/**
 *Executes the command for starting FlightGear. 
 * @param command an Array of Strings containing the path to the executable and the defined command line options. 
* @exception java.io.IOException IOException.
* @exception java.lang.NullPointerException NullPointerException.
* @exception java.lang.IllegalArgumentException IllegalArgumentException.
 */
	public void executeCommand(String[] command) throws IOException, NullPointerException,  IllegalArgumentException {
		
		Process p = Runtime.getRuntime().exec(command);	

		//Creates a new DebugOutput object for FlightGears terminal output.
		t = new Thread(new DebugOutput(p));
		t.start();
		this.output = "\nStart:\n-----------------------------"+this.output;
	}

/**
 *Writes the configuration to the file associated with the path.
* @param command an Array of Strings containing the path to the executable and the defined command line options. 
* @param path full path to the configuration file.
 */
	public void saveConfig(String[] command, String path){
		if (path == null)path = Data.getDefaultConfig();
		command[0] = "--fg-execute="+this.execute;
		if (Data.getDataChanged() )this.output = "\nSave to:"+path+" \n-----------------------------"+this.output;
		else this.output = "\nNothing changed:\n-----------------------------"+this.output;
		
		// Writes FlightGear command line options to config file 
		Data.write(path, command, false);

		// Writes none FlightGear command line options to config file 
		Data.write(path, extra.toArray(), true);


		Data.setDataChanged(false);
	}

/**
*Returns the executed command.
* @return Returns a string representation of the object
*/
	public String toString(){
		return this.output;
	}


}
