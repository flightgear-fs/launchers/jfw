/**
 *Offers all the GUI components.
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/CheckBox.jpg">CheckBox Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/ButtonPanel.actionPerformed.jpg">ButtonPanel.actionPerformed Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/ParseProperties.parseConfig.jpg">ParseProperties.parseConfig Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/RadioButton.jpg">RadioButton Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/TextField.jpg">TextField.jpg Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/Panel.jpg">Panel.jpg Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/MenuOptions.jpg">MenuOptions Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/MenuList.Lauscher.mousePressed.jpg">MenuList.Lauscher.mousePressed Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/Menu.setMenu.jpg">Menu.setMenu Sequence Diagram</A>
* @see
*  <A HREF="../../uml/seq/Menu.removeMenu.jpg">Menu.removeMenu / Menu.update Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
 */
package src.comp;
