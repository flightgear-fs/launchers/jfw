/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    CheckBox.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/



package src.comp;

import javax.swing.JCheckBox;

import java.awt.Color;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

import src.obj.Data;



/**
* Menu component for swithing command line options on/off
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/CheckBox.jpg">CheckBox Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class CheckBox extends JCheckBox implements ItemListener,UI{           // for  settings-list gui-input

	String text, command;
	boolean selected;
	

/**
 *Initializes the Checkbox and add the ItemListener . 
 * @param text description 
* @param command command line option the Checkbox is associated with.
* @param  selected true or false.
*/	
	public CheckBox(String text, String command, boolean selected){
		this.text =text;
		this.selected = selected;
		this.command =command;
		setText(this.text);
		setOpaque(false);
		setForeground(Color.red);
		if (!this.selected)setForeground(Data.getFontColor());
		setSelected(this.selected);
		addItemListener(this);
	}


/**
 * Manages the values in the values HashMap dependent on the CheckBox is selected or not. 
 */	
	public void itemStateChanged(ItemEvent e){
		if (e.getStateChange() == ItemEvent.SELECTED){
			setForeground(Color.red);
			if (command.charAt(0)!='-' )
				Data.setValue(command, "--enable-");
			else 
				Data.setValue(command, command);
		}
		if (e.getStateChange() == ItemEvent.DESELECTED){
			setForeground(Data.getFontColor());
			if (command.charAt(0)!='-' )
				Data.setValue(command, "--disable-");
			else{
				Data.removeValue(command);
			}
		}
	}

/**
 *Update font color . 
*/
	public void updateFont(){
		setForeground(Data.getFontColor());
	}
}
