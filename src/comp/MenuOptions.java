/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


   MenuOptions.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.comp;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;

import javax.swing.JPanel;
import java.util.ArrayList;

import src.comp.Panel;


/**
*The MenuOptions class represents a created menu.
* All menu components and Add-On panels have to be added to a MenuOptions  JPanel.
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/MenuOptions.jpg">MenuOptions Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class MenuOptions extends JPanel{               

	private  final JPanel pluginspanel;
	private  final JPanel  settingspanel;
	private  final ArrayList<Panel> ap = new ArrayList<Panel>();
	private  final ArrayList<Panel> as = new ArrayList<Panel>();


/**
 * Initializes the  MenuOptions JPanel. 
 * @param dat an Array of objects containing the menu components initialized by the CreateMenu.create() method.
*/		
	public MenuOptions(Object[] dat){

		fillCollections(dat);
		
		setOpaque(false);
		pluginspanel = new JPanel(new GridLayout(ap.size(),1));
		settingspanel = new JPanel(new GridLayout(as.size(),1));
		pluginspanel.setOpaque(false);
		settingspanel.setOpaque(false);


		//places the addons to the Add-On JPanel
		for(Panel ip : ap){
			pluginspanel.add(ip);
		}


		//places the menu components to the settings JPanel
		for(Panel ip : as){
			settingspanel.add(ip);
		}


		// if an addon exists in this menu.
		if (ap.size() > 0){
			setLayout(new BorderLayout());
			add(pluginspanel, BorderLayout.CENTER);
			add(settingspanel, BorderLayout.SOUTH);
		}else {
			setLayout(new FlowLayout(FlowLayout.LEFT));
			add(settingspanel);
		}
	}


	// Seperates plugin panels from property panels.
	private void fillCollections(Object[] dat){


		for(Object o :  dat ){
			Panel p = (Panel)o;
			if (p.isPlugin())
				ap.add(p);
			else 
				as.add(p);
		}

	}


/**
 * Returns an  ArrayList containig all menu components except Add-On panels.
* @return  ArrayList<Panel>
*/			
	public ArrayList<Panel> getMenuOptions(){return this.as;}

/**
 * Returns an  ArrayList containig all Add-On panels.
* @return  ArrayList<Panel> or null
*/			
	public ArrayList<Panel> getAddOns(){
		if (this.ap.size() > 0)
			return this.ap;
		else
			return null;
	}


}
