/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    Menu.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.comp;

import java.awt.Component;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import java.util.ArrayList;

import src.obj.Data;
import src.JFlightWizard;

/**
* The JScrollPane on wich the MenuOptions panel (the menu) will be placed.
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/Menu.setMenu.jpg">Menu.setMenu Sequence Diagram</A>
* @see
*  <A HREF="../../uml/seq/Menu.removeMenu.jpg">Menu.removeMenu / Menu.update Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class Menu extends JScrollPane{

	private static MenuOptions active_menu;


/**
 * Initializes the Menu JScrollPane. 
 */
	public Menu(){

		this.setBorder(null);
		this.setOpaque(false);
		this.getViewport().setOpaque(false);
		
	}

/**
 * Creates a MenuOptions panel with the returned array of the CreateMenu().create method and adds it to the Menu JScrollPane. 
 * @param selected  a String representation of the selected Menu_List item (is the key for the menu HashMap in the Data object).
 */
	public void setMenu(String selected){

		
		// Removes current menu.
		 removeMenu(false);
	

		//Creates new MenuOptions JPanel. 
		active_menu = new MenuOptions(
							JFlightWizard.create_menu.create(
								Data.getMenu(selected).toArray()));

		// Sets the new  MenuOptions JPanel  as  current active_menu in the Data object.
		Data.setActiveMenu(active_menu);
		
		//Adds the AddOn buttons panels if exist .		
		addButtonsPanels();
		
		// places the new  MenuOptions JPanel.
		update();
	}

	private void update(){
		this.getViewport().add(active_menu);	
		active_menu.updateUI();
		 JFlightWizard.panel_west.updateUI();
	}

	 
	private void addButtonsPanels(){
		ArrayList<Panel> plugin_panels; 
		JFlightWizard.resetButtonsLocation();
		if((plugin_panels =  active_menu.getAddOns()) != null){
			for (Panel p : plugin_panels){
				JPanel bp;
				if ((bp = p.getButtonsPanel()) != null)
					JFlightWizard.setAddOnButtons(bp);
			}
		}		
	}
/**
 * Removes the current MenuOptions JPanel, if it is not null and removes Add-On buttons panels, if  exists. 
*@param update clears the MenuList selection if true.
 */
	public void removeMenu(boolean update){

		// Current menu.
		active_menu = Data.getActiveMenu();
		if (active_menu != null){
			active_menu.removeAll();
			this.getViewport().removeAll();
			if (update){
				JFlightWizard.menu_list.clear();			
				update();
			}
		}
		//remove buttons panels.
		Component[] comp = JFlightWizard.panel_west.getComponents();
		for(Component c :comp){
			if(c instanceof JPanel && !(c instanceof ButtonPanel))
				 JFlightWizard.panel_west.remove(c);
		}
	}
}
