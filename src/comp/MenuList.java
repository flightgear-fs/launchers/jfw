/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    MenuList.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.comp;

import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import src.JFlightWizard;
import src.obj.Data;


/**
* A JList on a JScrollPane containing all available menus.
* A mouse click on a list item invokes the internal MouseAdapter to create a menu.
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/MenuList.Lauscher.mousePressed.jpg">MenuList.Lauscher.mousePressed Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class MenuList extends JScrollPane {
	


	private static  JList list = new JList();
	private String selected = null;

	class Lauscher extends MouseAdapter{            
		public void mousePressed(MouseEvent e) { 
			JList l = (JList)e.getSource();
			selected = (String) l.getModel().getElementAt(l.getSelectedIndex());
			JFlightWizard.menu.setMenu(selected);
		}
	}



/**
 * Creates the Menu_List containing the menu titles. 
 * @param dat  an Array of objects containing the keys (are the menu titles in the Menu_List) for  the  menu HashMap in the Data object.
* @param cr  CellRenderer for custom JList look and feel.  
*/
	public MenuList(Object[] dat, CellRenderer cr){	
		
		setOpaque(false);
		getViewport().setOpaque(false);
		setBorder(null);
		list.setListData(dat);
		list.setCellRenderer(cr);	
		list.setFixedCellWidth(175);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setOpaque(false);
		list.addMouseListener(new Lauscher());
		getViewport().add(list);
		
		
	}

/**
 *Clears  the MenuList selection . 
*/
	public void clear(){
		list.clearSelection(); 
	}


/**
 *Update font color . 
*/
	static public void updateFont(){
		list.setForeground(Data.getFontColor());
	}

/**
 *Returns a  String representation of the selected MenuList item . 
* @return String. 
*/
	public String getSelectedItem(){
		return selected;
	}
}
