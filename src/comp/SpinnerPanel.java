/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


   SpinnerPanel.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.comp;

import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpinnerDateModel;
import javax.swing.AbstractSpinnerModel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.awt.FlowLayout;
import java.awt.Component;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;

import src.obj.Data;

	

/**
* This Menu component provides one or more JSpinners for definig values consisting of numbers.
* For setting up  a SpinnerPanel a view  tags can/must be used in properties.txt file. 
* "$SPINNER$" instructs the CreateMenu().create() method to create a SpinnerPanel menu item. 
* The folowing tags can be used mixed and multible!
* "$INT:[min],[max],[step]$" defines the behavior of the spinner for integer values .
*"$DOUBLE:[min],[max],[step]$" defines the behavior of the spinner for double values.
* Where min defines the start value, max defines the end value and step the size value of each step.
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/TextField.jpg">TextField.jpg Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class SpinnerPanel extends JPanel implements ChangeListener{   
	
	private ArrayList<String[]> spinners = new ArrayList<String[]>();
	private Calendar cal;
	private String command;
	private String[]  linked_options = null;
	private int panel_x = 332;
	private int panel_y = 25;	
	private int size;
	
	//The JSpinner
	class Spinner extends JSpinner{
		Dimension d;
		public Spinner(AbstractSpinnerModel model, Dimension d){
			super(model);
			this.d = d;
			update();
			addChangeListener(SpinnerPanel.this);
		} 
		public void update(){
			setPreferredSize(d);
			((JSpinner.DefaultEditor) getEditor()).getTextField().setForeground(Data.getFontColor());
			((JSpinner.DefaultEditor) getEditor()).getTextField().setOpaque(false);
			getEditor().setOpaque(false);
			setOpaque(false);
			
		}
		
		public void updateFont(){
			((JSpinner.DefaultEditor) getEditor()).getTextField().setForeground(Data.getFontColor());
		}

	}


/**
 *Initializes the SpinnerPanel and setups the JSpinners .
 * @param value last configuration. 
* @param command command line option this spinners are associated with.
* @param items spinner model  parameters
*/	
	public SpinnerPanel(String value, String command , Object[] items){
			
		this.command = command.trim();	
		setPreferredSize(new Dimension( panel_x,  panel_y));
		setLayout(new FlowLayout(FlowLayout.LEFT));
		setOpaque(false);

		//filters spinner info and RadioButton info
		for (int i = 0; i < items.length; i++){

			 if (((String[])items[i])[0].equals("$BELONGTO$"))
				linked_options	= (String[]) items[i];
			else 
				spinners.add((String[]) items[i]);
		}


		//gets the number of JSpinners to be created
		size = spinners.size();
		


		//sets the dimension for the JSpinners
		Dimension spinner_dimension = new Dimension((panel_x/size)-6,  panel_y-5);

		//looks  for the last configuration values
		double[] current_value = getCurrentValues(value, size); 
	

		
		



		//creates and adds the JSpinners
		for (int i = 0; i < size; i++){
			
			String[] sp = spinners.get(i);

			if (sp[0].equals("$DATE$")){
				cal = Calendar.getInstance();
				if (command.contains("--extra-date")){
					
 					cal.set((int) current_value[0], (int) current_value[1], (int) current_value[2], (int) current_value[3], (int) current_value[4]); 
				}		
      					int param =Integer.parseInt(sp[1]);
					Date last_time = cal.getTime();

     					
					Spinner spinner = new Spinner(
											new SpinnerDateModel(
												last_time , null, null,param),
											spinner_dimension);
				
 					spinner.setEditor(new JSpinner.DateEditor(spinner, getFormat(param)));
					spinner.update();
					add(spinner);

				
			}
			else{

				double min = Double.parseDouble(sp[1]);
				double max = Double.parseDouble(sp[2]);
				double step =Double.parseDouble(sp[3]);

				//if the current value, found in the configuration, is not valid, reset the value to the minimum.
				if (current_value[i] < min)
					current_value[i] = min;

				if (sp[0].equals("$INT$"))
					add(new Spinner(
							new SpinnerNumberModel(
								(int) current_value[i], (int)  min, (int)  max, (int)  step), 
							spinner_dimension));
			
				else if (sp[0].equals("$DOUBLE$"))
					add(new Spinner(
							new SpinnerNumberModel(
								current_value[i], min, max, step), 
							spinner_dimension));
			}
			
		}
													
		
		
	}



	//sets the spinners start values to the values found in the config file or to 0.
	private double[] getCurrentValues(String v, int size){
		double[] cv = new double[size];
		if (v != null){
			String[] tmp = v.split(":");
			for (int i = 0; i < size; i++){
				cv[i] = Double.parseDouble(tmp[i]);
			}	
		}		
		else{
			for (int i = 0; i < size; i++){
				cv[i] = 0;
			}
		}
		return cv;
	}

	
		
	private String getFormat(int n){
		if (n ==1) return "yyyy";
		else if (n ==2) return "MM";
		else if (n ==5) return "dd";
		else if (n ==10) return "HH";
		else if (n ==12) return "mm";
		else return "ss";
	}







/**
*Invokes on every  Spinner value changed event.
 */	
	public void stateChanged(ChangeEvent e)  {
			//double[] values=null;
		double[] values= getValues();	
		
		//changes fontcolor
		if (command.equals("--extra-fontcolor="))
			Data.setFontColor((int) values[0], (int) values[1], (int) values[2]);

		
		

			//creates the value string
			String value = "";
			for (int i = 0 ; i < values.length; i++){
				value = value+values[i]+":";	
			}
System.out.println(value);
			//removes  every substring like .0
			value = value.replaceAll(".0:", ":");
			value = value.substring(0, value.length() - 1);
System.out.println(value);
			//If this SpinnerPanel belongs to a ButtonGroup
			if (linked_options != null){
				for(int i = 1 ; i < linked_options.length; i++ ){
					if (Data.containValueKey(linked_options[i]))
						Data.setValue(linked_options[i], value);
				}
			}
			
			Data.setValue(command, value);

		
	}





/**
* Returns all  the values from all JSpinners.
 *@return an Array of double
 */
	public double[] getValues(){

		 double[]  values  = new double[size];
		Component[] comp = getComponents();

		Component c = null;
		try {
			for( int i = 0; i < values.length; i++){
    				c =comp[i];
				if(c instanceof JSpinner){
					Spinner s = ((Spinner) c);
					s.commitEdit();
					s.updateFont();
					if (s.getModel() instanceof SpinnerDateModel){
	   					SimpleDateFormat formatter =  
							new SimpleDateFormat(
								getFormat(
									Integer.parseInt(
										spinners.get(i)[1])));

						values[i] = Double.parseDouble(  
										formatter.format(
											s.getValue()));

					}

					else if (s.getModel() instanceof SpinnerNumberModel)
						values[i] = Double.parseDouble( s.getValue().toString());

				}
			}
   		}
		catch (ParseException pe) { }
   				
		return values;
	}





}









		
