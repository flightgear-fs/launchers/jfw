/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


   BrowseTextField.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/



package src.comp;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.FlowLayout;
import java.io.File;

import src.JFlightWizard;
import src.obj.Data;



/**
* This Menu component  provides a dialogue for browsing the filesystem and setting one or more values to one textfield.
* For customizing the dialogues behaviour it is possible to use 3 tags in properties.txt file. 
* "$BROWSE$" instructs the CreateMenu().create() method to create a BrowseTextField menu item. 
*"$SEP$" instructs  an BrowseTextField menu item to handl more than one value in one TextField.
*"$ONLYDIR$" instructs the dialogue to show only directories.
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/TextField.jpg">TextField.jpg Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class BrowseTextField extends JPanel implements ActionListener,UI{   
	
	private JButton browse = new JButton("Browse");
	private TextField textfield;
	private JFileChooser file_chooser = new JFileChooser();
	private String command;
	private boolean multi;
	private boolean only_dir;
	private Object[] file_filter;





/**
 *Initializes the BrowseTextField JPanel, creates a TextField and adds the ActionListener and the configuration value to it .
 * @param text value. 
* @param command command line option this TextField is associated with.
* @param  columns length of this TextField.
* @param multi if true, more than one value could be defined separated by the OS dependend path separator or by "&" .
* @param  only_dir if true, only directories will be shown by the file chooser dialogue.
*/	
	public BrowseTextField(String text, String command ,int columns, boolean multi, boolean only_dir, Object[] file_filter){
		this.command = command;		
		this.multi = multi;
		this.only_dir = only_dir;
		this.file_filter = file_filter;
		setLayout(null);
		setOpaque(false);
		
		//Creates a TextField, sets a custom Layout and adds it to this Component.
		textfield = new TextField(text, command, columns);


		//Load file filters	
		setFileFilter();

		textfield.setBounds(4,0,332,18);
		browse.setBounds(340,0,80,18);
		add(textfield);
		add(browse);
		browse.addActionListener(this);
	}

	//Load file filters associated with the this.command.
	private void setFileFilter(){

		//switch off all file filter.		
		file_chooser.setAcceptAllFileFilterUsed(false);

		//Sets the file chooser dialogue to directories only.
		if (this.only_dir)
			file_chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			
		else if (file_filter.length > 0){
			for(int i = 0; i < file_filter.length; i++){
				String[] tmp = (String[]) file_filter[i];
				file_chooser.setFileFilter(new FileNameExtensionFilter((String) tmp[1], (String) tmp[2]));
			}
			
		}
		else 
			file_chooser.setAcceptAllFileFilterUsed(true);
	}



	//updat GUI components associated with this.command. 
	private void updateComponent(){
		//Update main window
		if (this.command.equals("--extra-background="))
			JFlightWizard.repaintBackground();
	}



	//Returns the current value of the TextField
	private String getValue(){
		return this.textfield.getValue();
	}
	
	

	


/**
* Provides a dialogue for browsing the filesystem.
 * Invokes after the Browse JButton was pressed.
 */	
	public void actionPerformed(ActionEvent e) {
		String co = e.getActionCommand();
		if(co.equals("Browse")){



			//Sets the file chooser default directory to the  value from the TextField if the location exists.
			if (Data.containValueKey(getCommand())){
				String s = Data.getValue(getCommand());
				if (s.contains(Data.PSEP)){
					String[] tmp = s.split(Data.PSEP);
					s = tmp[0].trim();				
				}
				File f = new File(s);
				if (f.exists())
					file_chooser.setCurrentDirectory(f);
			}





			//Sets the file chooser default directory to the defaultsavepath value  if the value or the location exists.
			else if(Data.containValueKey("--extra-defaultsavepath=")){
				File f = new File(Data.getValue("--extra-defaultsavepath="));
				if (f.exists())
					file_chooser.setCurrentDirectory(f);
			}			
			



			//Sets the file chooser default directory to the default config  save path (JFlightWizard root directory).
			else
				file_chooser.setCurrentDirectory(
								new File(Data.getDefaultConfig()));





			int returnVal = file_chooser.showOpenDialog(BrowseTextField.this);
			if (returnVal == JFileChooser.APPROVE_OPTION){
				String s =getValue();
				s = s.replaceAll(" ", "");
			
				//if there are more than one  value allowed on this option, values will be concatenate and separated by the OS dependend path separator.
				if (this.multi && !s.equals(""))
					s = getValue().concat(
						Data.PSEP+
							file_chooser.getSelectedFile().getPath());
				else
					s = file_chooser.getSelectedFile().getPath();	
				
				//Sets the new value to the value HashMap 
				Data.setValue(getCommand(), s);
				
				//Sets the value to the TextField
				setValue(s);

				//Update components if this.command affects  components
				updateComponent();
		
			}	

		}
	}





/**
 *Sets a new value to the TextField. 
 * @param s value. 
*/	
	public void setValue(String s){
		this.textfield.setValue(s);
	}
	
	/**
 *Update font color . 
*/
	public void updateFont(){
		this.textfield.setForeground(Data.getFontColor());
	}

/**
 *Returns the command line option this  TextField is associated with. 
 * @return command . 
*/	
	public String getCommand(){return textfield.getCommand();}
}
