/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


   Panel.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.comp;

import javax.swing.JPanel;
import javax.swing.JComponent;
import java.awt.LayoutManager;
import java.awt.Component;


import src.obj.Data;


/**
* Every menu component have to be  represented by  a Panel.
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/Panel.jpg">Panel.jpg Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class Panel extends JPanel{                    
	private boolean isplugin;
	private JPanel buttons_panel = null;


/**
 *Default constructor 
 * @param layout     sets the specified layout manager for this Panel.


*/	
	public Panel(LayoutManager layout){
		super(layout);
	}

/**
 *Initializes the Panel. 
 * @param layout  sets the specified layout manager for this Panel.
* @param  isplugin   true if it is a AddOn  or  false if it is not.
*/	
	public Panel(LayoutManager layout, boolean isplugin){
		super(layout);
		this.isplugin = isplugin;
		setOpaque(false);
	}



/**
 *For checking whether this Panel contains a AddOn or not. 
 * @return true if this Panel contains a AddOn, else false.
*/	
	protected boolean isPlugin(){return this.isplugin;}

/**
 *Returns a button panel or null if this Panel contains an AddOn . 
 * @return JPanel.
*/	
	protected JPanel getButtonsPanel(){
		return  this.buttons_panel;
	}


/**
 *Sets an AddOn buttons panel . 
 * @param bp JPanel.
*/	
	public void setButtonsPanel(JPanel bp){
		this.buttons_panel = bp;
	}

/**
 *Update font color . 
*/
	public void updateFont(){
		Component[] c = getComponents();
	
		if (c[0] instanceof UI)
			((UI) c[0]).updateFont();

		 c[c.length-1].setForeground(Data.getFontColor());
		
	}

}
