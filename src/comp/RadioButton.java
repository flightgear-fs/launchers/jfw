/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


   RadioButton.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.comp;


import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

import java.awt.Color;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

import src.obj.Data;



/**
* Menu component for ButtonGroups
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/RadioButton.jpg">RadioButton Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class RadioButton extends JRadioButton implements ItemListener, UI{           // for  settings-list gui-input
	
	String text, command, spinner;
	boolean selected;
	ButtonGroup bg;



/**
 *Initializes the RadioButton, add the ItemListener and places the component to the ButtonGroup . 
 * @param text Description 
* @param command command line option the RadioButton is associated with.
* @param  selected true if this RadioButton is selected, else  false.
* @param bg  ButtonGroup on wich this RadioButton depends.
*/	
	public RadioButton(String text, String command, boolean selected, ButtonGroup bg, String spinner){
		this.text =text;
		this.selected = selected;
		this.command = command;
		this.spinner = spinner;
		this.bg = bg;
		this.bg.add(this);
		setText(this.text);
		setOpaque(false);
		setForeground(Color.red);
		if (!this.selected)setForeground(Data.getFontColor());
		setSelected(this.selected);
		if (this.selected)doClick();
		addItemListener(this);
	}




/**
 * Manages the values in the values HashMap dependent on whether  the RadioButton is selected or not. 
 */	
	public void itemStateChanged(ItemEvent e){
		if (e.getStateChange() == ItemEvent.SELECTED){
			setForeground(Color.red);
			 if (spinner != null)
				Data.setValue(command, Data.getValue(spinner));
			else
				Data.setValue(command, command);
		}
		if (e.getStateChange() == ItemEvent.DESELECTED){
			setForeground(Data.getFontColor());
			Data.removeValue(command);
		}

	}
/**
 *Update font color . 
*/
	public void updateFont(){
		setForeground(Data.getFontColor());
	}
}
