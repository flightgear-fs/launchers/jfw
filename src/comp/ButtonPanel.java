/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    ButtonPanel.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.comp;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.io.IOException;
import java.io.File;

import src.JFlightWizard;
import src.obj.Data;


/**
* Provides the buttons and dialogue for starting FlightGear, saving and  loading  configuration. 
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/ButtonPanel.actionPerformed.jpg">ButtonPanel.actionPerformed Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/ParseProperties.parseConfig.jpg">ParseProperties.parseConfig Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class ButtonPanel extends JPanel implements ActionListener{

	private  JButton run = new JButton("Run");
	private  JButton save = new JButton("Save");
	private  JButton load = new JButton("Load");
	static private JFileChooser file_chooser = new JFileChooser();
	private String output = "";



/**
 * Initialize buttons and  the dialog for loading and saving configurations. 
 */
	public ButtonPanel(){
	
		setLayout(null);
		setOpaque(false);
		
		load.setBounds(2,2, 72, 22);
		save.setBounds(72,2, 72, 22);
		run.setBounds(2,  22, 142, 37);
		add(load);
		add(save);
		add(run);
		setBorder(BorderFactory.createLineBorder(Color.gray,1));

		//set flight plane file filter
    		file_chooser.setFileFilter(new FileNameExtensionFilter("FlightGear route ( .fgr )", "fgr"));
		
		//set configuration file filter
		file_chooser.setFileFilter(new FileNameExtensionFilter("FlightGear configuration file  ( .fgc ) ", "fgc"));
	
		//switch off all file filter.		
		file_chooser.setAcceptAllFileFilterUsed(false);
 
		run.addActionListener(this);
		save.addActionListener(this);
		load.addActionListener(this);
	}

/**
* Invokes when a button was pressed.
* "Run" executes command for starting up FlightGear.
* "Save" brings up a dialogue for saving the configuration to a spicified file.
* Saving the configuration to the config.fgc file in the JFlightWizard root directory always occures when the "Save" button or the "Run" button was pressed.
* This config.fgc file is used to initialize the configuration values of the last session  at JFlightWizards startup.
*"Load" button provides a dialogue for loading specified configuration files. 
* This method also prints the executed and/or saved command line options to stdout.  
*/
	public void actionPerformed(ActionEvent e) {             
		String co = e.getActionCommand();	
		try{
			
			//set default path for file chooser
			if (Data.getValue("--extra-defaultsavepath=") != null)
				file_chooser.setCurrentDirectory(
					new File(Data.getValue("--extra-defaultsavepath=")));
			else
				file_chooser.setCurrentDirectory(new File(Data.getDefaultConfig()));

			//create command
			String[] command = JFlightWizard.start_flight.createCommand();
			
			
			if (co.equals("Run"))

				//Executes command
				JFlightWizard.start_flight.executeCommand(command);
			
			if (co.equals("Save") || co.equals("Run")) {

				//Saves default config 
				JFlightWizard.start_flight.saveConfig(command, Data.getDefaultConfig());
				
				//Saves	 config to a specified place.	
				if (co.equals("Save")){
					int returnVal =file_chooser.showSaveDialog(ButtonPanel.this);
					if(returnVal == JFileChooser.APPROVE_OPTION){
						Data.setDataChanged(true);
						
						//set new default path for file chooser 
						String path = file_chooser.getSelectedFile().getParentFile().getPath();
						Data.setValue("--extra-defaultsavepath=", path);
						Data.setTextFieldValue("--extra-defaultsavepath=", path);

						//recreate command to update --extra-defaultsavepath=
 						String[] command2 = JFlightWizard.start_flight.createCommand();

						//Saves	config to a specified file
						String s = file_chooser.getSelectedFile().getPath();
						if (!s.endsWith(".fgc"))s = s.concat(".fgc");
						JFlightWizard.start_flight.saveConfig(command2, s);
						
					}
				}	
				
				this.output = this.output.concat( "\n"+JFlightWizard.start_flight.toString()+"\n");
			}
	
			//Sets new configuration values found in the specified file
			else if (co.equals("Load")) {
				int returnVal = file_chooser.showOpenDialog(ButtonPanel.this);
				if (returnVal == JFileChooser.APPROVE_OPTION){
					
					//Initialize new values
					String s = file_chooser.getSelectedFile().getPath();
					if (s.endsWith(".fgc"))
						JFlightWizard.parse_properties.parseConfig(s);
					
					//reset menu
					JFlightWizard.menu.setMenu(JFlightWizard.menu_list.getSelectedItem());

					//print new config to stdout
					this.output = this.output.concat( 
						"\n"+JFlightWizard.parse_properties.toString()+"\n");
				}
           		}	
		}
	
		catch (IOException x1){this.output = this.output.concat("I/O error \n"+x1);}
		catch (NullPointerException x2){this.output = this.output.concat("command is null\n"+x2);}
		catch (IllegalArgumentException x3){this.output = this.output.concat("command is illegal\n"+x3);}
	
		//prints executed and/or saved command line options to stdout
		System.out.println(this.output);
	}


}
