/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    CellRenderer.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.comp;

import javax.swing.JList;
import javax.swing.DefaultListCellRenderer;
import java.awt.Color;
import java.awt.Component;
import src.obj.Data;


/**
* A custom cellrenderer for managing font colors, transparency and borders of the JLists
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public  class CellRenderer extends DefaultListCellRenderer implements UI{
		
	public CellRenderer(){
		setOpaque(false);
		
	}

	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		if (!(value instanceof String)) return this;
		setBorder(null);
		if (isSelected) {
			setForeground(Color.red);
		} else {
			setForeground(Data.getFontColor());
		}
		return this;
	}

/**
 *Update font color . 
*/
	public void updateFont(){
		setForeground(Data.getFontColor());
	}
}
