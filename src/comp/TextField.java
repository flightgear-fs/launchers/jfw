/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


   TextField.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/



package src.comp;

import javax.swing.JTextField;
import javax.swing.BorderFactory ;

import java.awt.Color;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import src.obj.Data;
import src.JFlightWizard;


/**
* Menu component for adding text values.
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/TextField.jpg">TextField.jpg Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class TextField extends JTextField implements KeyListener,UI{   
	
	private int columns;
	private String text, command;



/**
 *Initializes the TextField, adds the KeyListener and the value this textField is associated with. 
 * @param text value. 
* @param command command line option this TextField is associated with.
* @param  columns length of this TextField.
*/	
	public TextField(String text, String command ,int columns){
		this.text =text;
		this.columns =columns;
		this.command =command;
		this.setCaretColor(Data.getFontColor());
		this.setEditable(true);
		this.setText(this.text);
		setColumns(this.columns);
		setOpaque(false);
		setBorder(BorderFactory.createLineBorder(Color.gray,1));
		setForeground(Data.getFontColor());
		addKeyListener(this);
	}



/**
 * Invokes on every key event, manages the values in the values HashMap dependent on whether the TextField  is empty or not. 
 */	
	public void keyReleased(KeyEvent e) {
		TextField t = (TextField) e.getSource();
		String co = t.getText();
		
		//Hiting ENTER in the  Background image TextField
		if(e.getKeyCode() == KeyEvent.VK_ENTER 
			&& command.equals("--extra-background="))
				JFlightWizard.repaintBackground();
		
		
		if(co.contains("&")){
			String[] tmp = co.split("&");
			co = tmp[0].trim();
			for (int i = 1; i < tmp.length; i++){
				co = co.concat(Data.PSEP+tmp[i].trim());	
			}
		}
		while (co.contains(" ")){
			co = co.replace(" ","");
		}
		if(!co.equals(""))
			Data.setValue(command, co);
		else 
			Data.removeValue(command);
	}

	public void keyTyped(KeyEvent e) {}
	public void keyPressed(KeyEvent e) {}



/**
 *Sets a new value to this TextField. 
 * @param s value. 
*/	
	public void setValue(String s){this.setText(s);}

/**
 *Returns a String representation of the  TextFields content. 
 * @return String. 
*/	
	public String getValue(){
		return this.getText();
	}

/**
 *Update font color . 
*/
	public void updateFont(){
		setForeground(Data.getFontColor());
	}

/**
 *Returns the command line option this  TextField is associated with. 
 * @return command . 
*/	
	public String getCommand(){return this.command;}
}
