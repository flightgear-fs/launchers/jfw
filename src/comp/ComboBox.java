/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


   ComboBox.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/



package src.comp;
import javax.swing.*;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.basic.ComboPopup;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.DefaultListCellRenderer;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Component;
import java.awt.Color;
import java.awt.Dimension;

import src.obj.Data;



/**
* This Menu component provides a repertory of values associated with this.command.
* For setting up  a ComboBox 2 tags must be used in properties.txt file. 
* "$COMBO$" instructs the CreateMenu().create() method to create a ComboBox menu item. 
*"$ITEMS:[value],[value],...$" defines the values to be shown in the ComboBox  menu item.
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/TextField.jpg">TextField.jpg Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class ComboBox extends JComboBox implements ActionListener,UI{   
	
	
	private String command;

	
		
	public  class Renderer extends DefaultListCellRenderer {
		
		public Renderer(){
			setOpaque(false);
			setPreferredSize(new Dimension(305, 13));
		}

		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
			super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			if (!(value instanceof String)) return this;
			setBorder(null);
			if (isSelected) {
				setForeground(Data.getFontColor());
			} else {
				setForeground(Color.black);
			}
			
			return this;
		}
	}



/**
 *Initializes the BrowseTextField JPanel, creates a TextField and adds the ActionListener and the configuration value to it .
 * @param text value. 
* @param command command line option this TextField is associated with.
* @param  columns length of this TextField.
* @param  repertory of values.
*/	
	public ComboBox(String value, String command ,Object[] items){
		super(items);
		
		this.command = command;	
		
		setFocusable(false);
		setRenderer( new Renderer());	
		setBackground(Color.gray);
		setForeground(Data.getFontColor());
		
		setOpaque(false);
	
	
		if (value != null && !command.contains("--multi"))
			setSelectedItem(value);
	
		else if (value != null && command.contains("--multi")){
			setEditable(true);
			addItem(value);
			setSelectedItem(value);
			setEditable(false);
		}
		else
			setSelectedItem("");
		

		addActionListener(this);
	}



	


/**
* Provides a dialogue for browsing the filesystem.
 * Invokes when a new value was choosen was pressed.
 */	
	public void actionPerformed(ActionEvent e) {
						
		//Sets the new value to the value HashMap 
		String selected = (String) getSelectedItem();
		if (selected == null)
			Data.removeValue(this.command);

		//if multible selection is set for this Combobox
		else if (command.contains("--multi")){
			String cv = Data.getValue(this.command);
			if (cv != null)
				selected = cv+Data.PSEP+selected;
			Data.setValue(this.command, selected);
			setEditable(true);
			addItem(selected);
			setSelectedItem(selected);
			setEditable(false);
		}
		else
			Data.setValue(this.command, selected);
			
	}
/**
 *Update font color . 
*/
	public void updateFont(){
		setForeground(Data.getFontColor());
	}
}









		
