/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


   AircraftList.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/




package src.addon;

import java.util.HashMap;
import javax.swing.JPanel;


import src.addon.aircraftlist.action.ParseAircraftData;
import src.addon.aircraftlist.comp.AircraftListPanel;
import src.addon.aircraftlist.comp.ButtonPanel;
import src.addon.aircraftlist.obj.Aircraft;



/**
*This class initializes all main components of the AircraftList Add-On.
*By inheriting all characteristics of the AbstractAddOn class and Data class, the comunication with the main application is provided.
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/TextField.jpg">TextField.jpg Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class AircraftList extends AbstractAddOn{

	
	static private final AircraftListPanel aircraft_list = new AircraftListPanel();
	static private final ButtonPanel button_panel = new ButtonPanel();
	static private final ParseAircraftData parse_data = new ParseAircraftData();
	static private HashMap<String, Aircraft> aircraft_data = new HashMap<String, Aircraft>(); 

/**
*This methode overrides the abstract getPanel() methode of the super class to return a JPanel representing the AircraftList extension. 
*First the aircraft data will be created or initialized.
*After that the  AircraftListPanel will be initialized with the data by calling the setList() method.
*On the last step,the AircraftListPanel  will   be returnt to the main application.
*@return JPanel the aircraft list
*/		
	public JPanel getPanel(){
		//Parsing the aircraftcache.txt file
		parse_data.readCacheFile();

		//Cretes an Arry of aircraft names.	
		Object[] aircrafts = getAircraftData().keySet().toArray();

		// If the aircraftcache.txt is empty or not exists a new aircraftcache.txt will be created like someone presses the refresch button.
		if (aircrafts.length < 1)
			 refreshAircraftCache();

		// If the aircraftcache.txt exists, setList() method will be called to add the aircraft names to the JList, provided by the AircraftListPanel class. 
		else
			aircraft_list.setList(aircrafts);

		return this.aircraft_list;
	}


/**
*This methode overrides the getButtonsPanel() methode of the super class to return a JPanel containing a JButton representing the refresh button.
*@return JPanel the button panel
*/		
	public  JPanel getButtonsPanel(){
		return this.button_panel;	
	}


/**
*A new aircraftcache.txt will be created by calling the refreshAircraftCache() method of the ParseAircraft class.
*Also the setList() method will be called to add the aircraft names to the JList provided by the AircraftListPanel class.
*/		
	static public void refreshAircraftCache(){
		parse_data.refreshAircraftCache();
		aircraft_list.setList(getAircraftData().keySet().toArray());
	}


/**
*Returns a HashMap containing the aircraft data 
*@return HashMap<String, Aircraft>  aircraft data
*/	
	static public HashMap<String, Aircraft> getAircraftData(){
		return aircraft_data;
	}

	

}


