/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


     AbstractAddOn.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.addon;


import javax.swing.JPanel;

import src.obj.Data;




/**
* This class inherit all characteristics of the Data class and provides implemented and abstract methodes for managing values and comunication with the main application.
* Add-Ons have to extend this class;
* @see 
*  <A HREF="../../uml/class/Extensions.jpg">Extensions  Class Diagram</A>
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public abstract class AbstractAddOn extends Data{	


/**
 *Must be overwritten to return a JPanel that represents the add-on. 
 * @return JPanel. 
 */	
	public abstract JPanel getPanel();



/**
 *Can be overwritten to return a JPanel that contains the buttons.
 * @return JPanel.  
 */	
	public  JPanel getButtonsPanel(){
		return null;	
	}

}
