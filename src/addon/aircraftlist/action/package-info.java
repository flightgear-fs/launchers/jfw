/**
 *Offers the extensions for JFlightWizard. 
* @see 
*  <A HREF="../../uml/class/Extensions.jpg">Extensions  Class Diagram</A>
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
 */
package src.addon.aircraftlist.action;
