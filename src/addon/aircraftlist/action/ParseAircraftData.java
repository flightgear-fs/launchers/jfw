/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    ParseAircraftData.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.addon.aircraftlist.action;

import java.util.ArrayList;
import java.io.File;

import src.addon.AircraftList;
import src.addon.aircraftlist.obj.Aircraft;




/**
*This class provides methodes to search for aircrafts, creating, updating and parsing the aircraftcache. 
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/TextField.jpg">TextField.jpg Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class ParseAircraftData{
	
	//Returns the full path to the aircraft directory.
	private String getAircraftPath(){
		return AircraftList.getValue("--fg-root=")+AircraftList.FSEP+"Aircraft";
	}

	//Returns the full path to the aircraftcache.txt file.
	private String getAircraftCache(){
		return AircraftList.UD+AircraftList.FSEP+"aircraftcache.txt";
	}

	private void clearAircraftData(){
		AircraftList.getAircraftData().clear();
	}

	//Creates an Aircraft object for every Aircraft and adds it to the aircraft_data HashMap.
	private void setAircraftData(String[] v){
		AircraftList.getAircraftData().put(
									v[0].trim(),
										new Aircraft(
												v[0].trim(),
													v[1].trim(), 
														v[2].trim())
									);
	}



/**
*Parses the aircraft.txt file and fills the aircraft_data HashMap by calling the setAircraftData() methode.
*/		
	public void readCacheFile(){
		ArrayList<String> al = AircraftList.read(getAircraftCache(), null);
		if (al != null){
			for(String s : al ){
				String[] tmp = s.split("#");
				if (tmp.length == 3)
					setAircraftData(tmp);
			}
		}
	}




/**
*Searches the aircraft directory relative to the fg-root directory, parses the -set.xml files aircrafts, fills the aircraftcache.txt file and the  aircraft_data HashMap with the data. 
*/	
	public void refreshAircraftCache(){                        
 		String sep = " # ";
		//clear HashMap
		clearAircraftData();
		ArrayList<String> cachdata = new ArrayList<String>();
		
		 // defines values to find information in the aircrafts -set.xml file
		String[] find = {"<description>"};   
		String[] cont = new String[find.length];
		File file = new File(getAircraftPath());
		// list all files and directories in the aircraft directory if it exists.
		if (file.exists()){
			File[] fi = file.listFiles();
			
			//searches every aircraft directory for the -set.xml file.
			for (File f : fi){
				if(f.isDirectory()){
					File[] afi = f.listFiles();

					for (File af : afi){
						if (af.getName().endsWith("-set.xml") && 
							!af.getName().equals("carrier-set.xml")){

							
								
								// uses a substring of the full -set.xml file name as real aircraft name.
								String startcommand = af.getName().substring(0, af.getName().lastIndexOf("-set.xml"));
								
								// parses the -set.xml file of the aircraft.								
								ArrayList<String> al =AircraftList.read(af.getPath(), find);
								
								for(String s : al){
									for (int i = 0; i < cont.length; i++){ 
										if (s.contains(find[i]))
											cont[i] = s.substring(
														s.indexOf(">")+1,  
															s.lastIndexOf("<"));
									}
								}


								//if nothing was found in the aircrafts -set.xml file.
								if (cont[0] == null)
									cont[0]= startcommand;

								//creates the Aircraft objects.
								String[] v = {cont[0],startcommand, f.getPath()};
								
								setAircraftData(v);

								//fills the cachdata ArrayList with Strings
								cachdata.add(cont[0]+sep+startcommand+ sep+f.getPath());
						}
					}
				}
			}

			// writes the cachdata content to the aircraftcache.txt
			AircraftList.write(getAircraftCache(), cachdata.toArray(), false);
			
		}
		else System.out.println(getAircraftPath()+" not exist!!");
	}
}
