/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


   Pic.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.addon.aircraftlist.obj;

import  javax.swing.ImageIcon;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Color;

/**
*The aircraft picture will be painted with doubled size on this JPanel.
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/TextField.jpg">TextField.jpg Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class Pic extends JPanel {
				
		private ImageIcon p;
		private Image i;
		private int x,y,width , height, borderthik ;
		
/**
*Initializes the size and the possition of the picture
*/
		public Pic(){

			x = 100;
			y =30;
			width =342;
			height = 256;
			borderthik = 3;
		}


/**
*Paints the border and the picture.
*/
		public void paintComponent(Graphics g){ 
			g.drawImage(i, x ,y,  width, height, this);
			g.setColor(Color.gray);
			
			for (int z = 2; z <= borderthik; z++ ){
				g.draw3DRect(x-z, y-z,  width+z+1, height+z+1, false);
			}	
		}


/**
*This methode has to be called for updating the JPanel with a new picture.
* Used by the mouse listener of the aircraft list.
*/
		public void setImage(String image_path){
			p = new ImageIcon(image_path);
			i = p.getImage();
			repaint();
		}
}
