/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    Aircraft.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/
package src.addon.aircraftlist.obj;

import src.addon.AircraftList;
/**
*An instance of this class is used to store information about one aircraft.
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/TextField.jpg">TextField.jpg Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class Aircraft{

	private String  path, command, name;

/**
*Initializes the Aircraft instance.
*@param command aircraft name to be used for the --aircraft command line option.
*@param path full path to the directory of the aircraft.
*/
	public Aircraft( String name, String command, String path){
		this.name = name;
		this.path = path;
		this.command = command;
	}

/**
*Returns the aircraft name showwn in the aircraft list.
*@return aircraft name is the key for the aircraftdata HashMap.
*/
	public String getName(){
		return this.name;
	}



/**
*Returns the value for the --aircraft command line option.
*@return the correct name of the aircraft.
*/
	public String getCommand(){
		return this.command;
	}


/**
*Returns the full path to the aircrafts picture.
*@return thumbnail.jpg.
*/
	public String getPath(){
		return this.path+AircraftList.FSEP+"thumbnail.jpg";
	}
} 
