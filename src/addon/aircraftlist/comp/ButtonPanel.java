/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    ButtonPanel.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/
package src.addon.aircraftlist.comp;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension ;
import java.awt.Color;

import src.addon.AircraftList;



/**
*Provides the refresh button as the ButtonPanel of this extension.
*The refresh button is used to update the aircraftcache.txt file if new plains are installed.
*Updating the aircraftcache.txt file occures automatical by selecting the AircraftList menu in the MenuList, if no cache file was found or if the file is empty
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/TextField.jpg">TextField.jpg Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class ButtonPanel extends JPanel implements ActionListener{


	private  JButton refresh = new JButton("Refresh");


/**
*Initializes the adds the refresh button and sets the layout.
*/	
	public ButtonPanel(){
		this.setLayout(null);
		this.setSize(new Dimension(146, 31));
		this.setOpaque(false);
		this.refresh.setBounds(2,  2, 142, 27);
		setBorder(BorderFactory.createLineBorder(Color.gray,1));
		this.refresh.addActionListener(this);		
		this.add(refresh);
		
	}

/**
*Invokes when the refresh button was pressed.
*Calles the refreshAircraftCache() method of the AircraftLisclass.
*/	
	public void actionPerformed(ActionEvent e) {
		String co = e.getActionCommand();
		if(co.equals("Refresh"))
			AircraftList.refreshAircraftCache();
	}
}
