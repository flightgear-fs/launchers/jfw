/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    AircraftListPanel.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/
package src.addon.aircraftlist.comp;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JList;
import  javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

import src.addon.AircraftList;
import src.addon.aircraftlist.obj.Pic;




/**
*Here the GUI components will be initialized and added to this JPanel.
*The internel MouseListener invokes if a JList item was selected.
*This causes the aircraft picture to be painted and the aircraft name to be added to the configuration as the choosen aircraft for FlightGears startup. 
* @see 
*  <A HREF="../../uml/class/MenuComponent.jpg">Menu components Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/TextField.jpg">TextField.jpg Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class AircraftListPanel extends JPanel{
		
	private JList aircraftlist = new JList();
	private JScrollPane sc = new JScrollPane(aircraftlist);
	private JLabel picture =new JLabel();
	private Pic picture_panel =new Pic();
	static private String selected = null;


	class aircraftlistLauscher  extends MouseAdapter{
		public void mousePressed(MouseEvent e) { 
			try{			
				JList l = (JList)e.getSource();

				//references the selected list item with the selected String variable.
				selected = (String) l.getModel().getElementAt(l.getSelectedIndex());
				
				//initializes the thumbnail of the aircraft.					
				picture_panel.setImage(AircraftList.getAircraftData().get(selected).getPath());
				
				//puts current selected aircraft to the values HashMap of the main application and the "aircraft=" TextField .  
				String value = AircraftList.getAircraftData().get(selected).getCommand();
				AircraftList.setValue("--aircraft=", value);
				AircraftList.setTextFieldValue("--aircraft=", value);
			}
			catch (NullPointerException  d) {System.out.println(d+"  not exists!!\n");}
		}
	}



/**
*Initializes the GUI components of the aircraft list.
*/	

	public AircraftListPanel(){
		
		this.setLayout(new BorderLayout());
		this.setOpaque(false);
		
		this.aircraftlist.setCellRenderer(AircraftList.getCellRenderer());
		this.aircraftlist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		this.sc.setOpaque(false);
		this.sc.getViewport().setOpaque(false);
		this.sc.setBorder(null);
		
		
		this.aircraftlist.setOpaque(false);
		this.aircraftlist.addMouseListener(new aircraftlistLauscher());
		this.add(this.sc, BorderLayout.WEST);
		this.add(this.picture_panel, BorderLayout.CENTER);
	}

/**
*Fills the aircraft list with the aircraft names located by the ParseAircraftData class.
*Also this methode chooses the first item in the list as the selected selected.
*/		
	public void setList(Object[] aircrafts){
		aircraftlist.setListData(AircraftList.sort(aircrafts));
		if (aircraftlist.getModel().getSize() > 0){
			//if the aircraftlist is not empty
			
				//for the latest selection
				 if (selected != null)
					aircraftlist.setSelectedValue(selected, true);

				//if no selection wos set in this session choos the aircrat from the last config
				else if(AircraftList.containValueKey("--aircraft=")){
					Object[] keys = AircraftList.getAircraftData().keySet().toArray();
					Object ac = AircraftList.getValue("--aircraft=");
					for (Object k : keys){
						if  (AircraftList.getAircraftData().get(k).getCommand().equals(ac)){
							selected = AircraftList.getAircraftData().get(k).getName();
							aircraftlist.setSelectedValue(selected, true);
						}
					}
				}

				//if no selection wos set in this session and no aircraft wos found in the loadet configuration choos the first list item.
				else{
					selected =  (String)aircraftlist.getModel().getElementAt(0);
					aircraftlist.setSelectedIndex(0); 
					AircraftList.setValue("--aircraft=", AircraftList.getAircraftData().get(selected).getCommand());
				}
				picture_panel.setImage(AircraftList.getAircraftData().get(selected).getPath());
				sc.getViewport().revalidate();
			
		}
	}
}
