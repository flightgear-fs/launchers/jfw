/**
 *Offers objects for handling the command line options and configuration.
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/class/Extensions.jpg">Extensions  Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/Data.read.jpg">Data.read Diagram</A>
* @see 
*  <A HREF="../../uml/seq/Data.sort.jpg">Data.sort Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/Data.write.jpg">Data.write Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
 */
package src.obj;
