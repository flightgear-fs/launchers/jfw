/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


   PropertiesObject.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/


package src.obj;

import src.action.GetPlugin;
import javax.swing.ButtonGroup;


/**
*  Every instance of this class will be created by ParseProperties and contains information for one menu component.
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/class/Extensions.jpg">Extensions  Class Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class PropertiesObject { 
  
	private String text, command, fgcommand, spinner;
	private GetPlugin p;
	private ButtonGroup bg;
	private boolean multi, onlydir;
	private Object[] items, file_filter;
	
/**
*Initializes PropertiesObject for TextField  and CheckBox components.
* @param command Command line option.
* @param text  Description.
*/
	public PropertiesObject(String command,  String text){
		this.command = command;
		this.text = text;
	}




/**
*Initializes PropertiesObject for  for AddOn panel.
* @param text  Description.
* @param p GetPlugin object
*/
	public PropertiesObject(String text, GetPlugin p){
		this.text = text;
		this.p = p;
		this.command = "plugin";
	}



/**
*Initializes PropertiesObject for  BrowseTextField .
* @param command command line option.
* @param db dialogue behaviour.
* @param text description.
*/
	public PropertiesObject(String command, boolean[] db, Object[] file_filter, String text){
		this.text = text;
		this.fgcommand = command;
		this.multi = db[0];
		this.onlydir = db[1];
		this.command = "browstextfield";
		this.file_filter = file_filter;
	}


/**
*Initializes PropertiesObject for ComboBox or SpinnerPanel.
* @param command command line option.
* @param items Array containing items for the ComboBox.
* @param text description.
*/
	public PropertiesObject(String command, String fgcommand, Object[] items, String text){
		this.text = text;
		this.fgcommand = fgcommand;
		this.items = items;
		this.command = command;
		this.bg = bg;
	}


/**
*Initializes PropertiesObject for  RadioButtons, ButtonGroup and the SpinnerPanel if the RadioButton belongs to a JSpinner.
* @param command command line option.
* @param spinner Spinner the Radiobutton is associated with.
* @param text description.
* @param bg the ButtonGroup the RadioButton is associated with.
*/
	public PropertiesObject(String command, String spinner, String text, ButtonGroup bg){
		this.text = text;
		this.fgcommand = command;
		this.spinner = spinner;
		this.command = "radiobutton";
		this.bg = bg;
	}

/**
*Returns the description, the command line option is associated with.
* @return description.
*/
	public String getText(){return this.text;}


/**
*Returns a String representation of the  commandline option or "plugin"/"radiobutton" if this instance contains infomation of a Add-on/RadioButton.
* @return command.
*/
	public String getCommand(){return this.command;}


/**
*Returns a String representation of the  commandline option if the property  associates  a RadioButton, else null.
* @return command or null.
*/
	public String getFGCommand(){return this.fgcommand;}


/**
*Returns the key for the values HashMap if this RadioButton belongs to a SpinnerPanel, else  null.
* @return Spinner or null.
*/
	public String getSpinner(){return this.spinner;}


/**
*Returns a ButtonGroup  if this instance associates a RadioButton, else null.
* @return ButtonGroup or null.
*/
	public ButtonGroup getButtonGroup(){return this.bg;}


/**
*Returns a GetPlugin object  if this instance associates a Add-On, else null.
* @return GetPlugin  or null.
*/
	public GetPlugin getPlugin(){return this.p;}


/**
*Returns true if the component is able to specify multible values.
* @return true if able to specify multible values.
*/
	public boolean isMulti(){return this.multi;}


/**
*Returns  true if the components dialogue should only show directories.
* @return if true dialogue only shows directories.
*/
	public boolean isOnlyDir(){return this.onlydir;}

/**
*Returns an Array of Strings containing the items found in properties.txt.
* @return  items for the combobox.
*/
	public Object[] getItems(){return this.items;}

/**
*Returns an Array of parameters for creating the file filters for the file chooser dialogue.
* @return  items for the combobox.
*/
	public Object[] getFileFilter(){return this.file_filter;}

/**
*Sets a new command to this PropertiesObject.
 * @param command new command.
*/
	public void setCommand(String command){this.command = command;}
}

