/***********************************************************************

    Written by Guenther Neuwirth, started  March 2008.
    Copyright (c) 2008   Guenther Neuwirth - ugulf@users.sourceforge.net


    Data.java is part of JFlightWizard.

    JFlightWizard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JFlightWizard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JFlightWizard.  If not, see <http://www.gnu.org/licenses/>.

***********************************************************************/



package src.obj;
import java.util.HashMap;
import java.util.ArrayList;
import java.io.File;
import java.awt.Component;
import java.awt.Color;

import src.comp.CellRenderer;
import src.comp.MenuOptions;
import src.comp.MenuList;
import src.comp.TextField;
import src.comp.Panel;
import src.action.ReadWriteFile;
import src.action.Sort;



/**
* Static Data object is used to store and provide all available properties, defined values, system dependent values and common objects.
* @see 
*  <A HREF="../../uml/class/JFlightWizard.jpg">JFlightWizard Class Diagram</A>
* @see 
*  <A HREF="../../uml/seq/Data.read.jpg">Data.read Diagram</A>
* @see 
*  <A HREF="../../uml/seq/Data.sort.jpg">Data.sort Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/seq/Data.write.jpg">Data.write Sequence Diagram</A>
* @see 
*  <A HREF="../../uml/Notes.txt">UML NOTES</A>
* @see 
*  <A HREF="../../../../RELEASE-NOTES">RELEASE-NOTES</A>
* @see 
*  <A HREF="../../../../README">README</A>
*/	
public class Data{

	static private final HashMap<String, ArrayList<PropertiesObject>>  menu = new HashMap<String, ArrayList<PropertiesObject>>(); //
	static private final HashMap<String, String> values = new HashMap<String, String>();
	static private final CellRenderer cellrenderer = new CellRenderer();
	static private final  ReadWriteFile rwf = new ReadWriteFile();
	static private final Sort sort = new Sort();
	
/** Name of the operating system.*/
	static public final String OSN = System.getProperty("os.name");

/** Version of the operating system.*/
	static public final String OSV = System.getProperty("os.version");

/** Root directory of JFlightWizard.*/
	static public final String UD= System.getProperty("java.class.path");

/** Users home directory.*/
	static public final String UH= System.getProperty("user.home");

/** User name.*/
	static public final String UN = System.getProperty("user.name");

/** File separator.*/
	static public final String FSEP = System.getProperty("file.separator");

/** Path separator.*/
	static public final String PSEP = System.getProperty("path.separator");

/**Line separator.*/
	static public final String LSEP = System.getProperty("line.separator");

/** Temp directory.*/
	static public final String TMPDIR = System.getProperty("java.io.tmpdir");
	
	 static private boolean data_changed =  false;
	 static private MenuOptions active_menu = null;


/**
* Returns the path of the default config.fgc .  
* @return  path to default config.fgc.
*/
	static public String getDefaultConfig(){
		return UD+FSEP+"config.fgc";
	}


/**
* Sets data_changed variable to b.  
* @param b true or false.
*/	
	static public void setDataChanged(boolean b){
		data_changed = b;
	}



/**
* Returns true if one or more properties value has changed else false.  
* @return true if one or more properties value has changed.
*/
	static public boolean getDataChanged(){
		return data_changed;
	}



/**
* Sets active_menu to  am.  
* @param am  one instance of MenuOptions  or null.
*/
	 static public void setActiveMenu(MenuOptions  am){
		active_menu = am;
	}



/**
*Returns the current viewed MenuOptions panel  or null  if there was nothing selected in the MenuList before.
* @return Returns the active menu.
*/
	static public MenuOptions getActiveMenu(){
		return active_menu;
	}



/**
* Associates the specified  ArrayList<Properties_Object> with the specified key in the menu HashMap.
* @param key Key the ArrayList will associated with.
* @param o ArrayList<Properties_Object>.
*/
	static public void setMenu(String key, ArrayList<PropertiesObject> o ){
		menu.put(key,o);
	}



/**
*Returns an ArrayList<Properties_Object> associated with the key.
* @param key Key for the menu HashMap.
* @return The  ArrayList<Properties_Object> associated with the key if the key exists.
*/
	static public ArrayList<PropertiesObject> getMenu(Object key){
		return menu.get(key);
	}



/**
* Returns an Array containing all keys of the menu HashMap.
* @return Array of keys.
*/
	static public Object[]  getMenuKeys(){
		return menu.keySet().toArray();
	}


/**
*  Removes the mapping for the specified key from the menu HashMap if present.
* @param key key the value is associated with.
*/
	static public void removeMenu(Object key){
		values.remove(key);
		data_changed = true;
	}


/**
* Associates the specified  String  with the specified key in the values HashMap.
* @param key key the value is associated with.
* @param v value.
*/
	static public void setValue(String key, String v){
		values.put(key, v);
		data_changed = true;
	}



/**
*  Removes the mapping for the specified key from the values HashMap if present.
* @param key key the value is associated with.
*/
	static public void removeValue(Object key){
		values.remove(key);
		data_changed = true;
	}
/**
*   Removes all mappings from this map.
*/
	static public void clearValue(){
		values.clear();
	}

/**
*Returns the value associated with the key.
* @param key key for the values HashMap.
* @return the value associated with the key if the key exists.
*/
	static public String  getValue(Object key){
		return values.get(key);
	}



/**
*Returns true if the values HashMap contains the key, otherwise false.
* @return true if values contains key else false.
*/
	static public boolean  containValueKey(Object key){
		return values.containsKey(key);
	}




/**
* Returns an Array containing all keys of the  values HashMap.
* @return Array of keys.
*/
	static public Object[]  getValueKeys(){
		return values.keySet().toArray();
	}



/**
*Writes the content of data into a file.
* @param path full path to the file.
* @param data  an Array of Strings containing the data to be written into the file.
* @param append if true the new content will be append to the file, else the old content in the file will be overwritten.
*/
	static  public void  write(String path, Object[] data, boolean append){
		if (data_changed|| !(new File(path).exists())) rwf.write(path, data,  append);
	}



/**
*Reads a file and returns every line containing one or more Strings defined in the Array.
*If the array is null the whole file content will be returned.
* @param pf full path of the file.
* @param toread  Array of Strings defining the return result.
* @return ArrayList<String> contains the result Strings
*/
	 static public ArrayList<String>  read(String pf, String[] toread){
		return rwf.read(pf, toread);
	}

/**
*Unzips a file  (tmporarly)
* @param p file.
* @return unzipt file
*/
	static public File unzip(File p) {
		return rwf.unzip(p);
  	}


/**
*Sorts Strings dependent on the first character.
* @return Array of sorted objects. 
*/
	static public Object[]  sort(Object[] data){
		return sort.sort(data);
	}


/**
*Order Strings if the first characters are integer values.
* @return Array of ordered objects. 
*/
	static public Object[]  order(Object[] data){
		return sort.order(data);
	}

/**
*Returns a custom CellRenderer for managing look and feel of JLists.
* @return CellRenderer.  
*/	
	static public CellRenderer  getCellRenderer(){
		return cellrenderer;
	}


/**
 *Sets configuration values to TextField.
*@param option String representation off the command line option.
*@param value String representation off the command line option value.
 */	
	static public  final void setTextFieldValue(String option, String value){
		ArrayList<Panel> menu_options =  getActiveMenu().getMenuOptions();
		for (Panel p : menu_options){
			for(Component c: p.getComponents()){
				if(c instanceof TextField){
					TextField t = (TextField)c;
					if (t.getCommand().equals(option))t.setValue(value);			
				}
			}
		}
	}


/**
 *This method returns the current memory usage of the application. 
 */
	static public  final void showMem(){
		System.out.println("Memory: current "+
			Runtime.getRuntime().totalMemory()/1048576+"MB   max "+
				Runtime.getRuntime().maxMemory()/1048576+"MB   free "+
					Runtime.getRuntime().freeMemory()/1048576+"MB");
	}


	/**
 *Sets a RGB  color with the specified red, green, and blue values in the range (0 - 255)  
* @param r red values in the range (0 - 255).
* @param g green values in the range (0 - 255).
* @param b blue values in the range (0 - 255).
 */	
	static public void setFontColor( int r, int g, int b ){
		setValue("--extra-fontcolor=", r+":"+g+":"+b );
		MenuList.updateFont();
		ArrayList<Panel> update_fonts = getActiveMenu().getMenuOptions();
         	for(Panel c : update_fonts ){
			c.updateFont();
		}
	}


/**
 *Returns a RGB color with the  red, green, and blue values found in the configuration or green as the default.
* This methode gets the JComponent wich is showing the font as parameter.
* @return RGB Color.
 */		
	static public  Color getFontColor(){
		String fc = getValue("--extra-fontcolor=");

		//if no configuration is available
		if (fc == null ){
			fc = "0:255:0";
			setValue("--extra-fontcolor=", fc );
		}

		String[] f = fc.split(":");
		return new Color(
						Integer.parseInt(f[0]),
						Integer.parseInt(f[1]),
						Integer.parseInt(f[2])
						).brighter();
	}
}
